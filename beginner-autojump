It's not uncommon for new Linux users to consider learning how to work with the Command Line Interface, or CLI, when switching to Linux. Modern operating systems can be used entirely from within a graphical environment, but, historically, Linux requires a degree of CLI use. While different flavors of Linux required different levels of CLI mastery, even today, many users find the command line more robust than any graphical interface. But is learning it worth the effort? I believe so and have discovered that using a CLI often makes tasks much efficient than a GUI. There's a learning curve, but once you get the basics down, you'll wonder why you didn't learn it sooner. Taking this route, you’ll discover that working in a terminal requires the fundamental ability to navigate the filesystem.

Although there are many advanced techniques, scripting options, and numerous tricks you could learn, simply navigating constitutes a large portion of the picture. The need to navigate should feel familiar to every computer user, regardless of experience. Similar to how you navigate through Explorer in Windows or Finder in OS X, you need to "get to" a file before you can "do" something to it!

Usually, you would navigate in a terminal using the `cd` command. Sensibly abbreviated from "change directory," it does one thing well. However, using `cd` can get tedious if you have a decent amount of work in different files and folders. To navigate to a given directory, you would enter `cd <dir>`, or "go to this directory." See the image below for a basic example.

_Below is an example of filesystem navigation using the `cd` command_.

Advanced terminal enthusiasts might not agree, but taking control of an operating system via CLI can be challenging and even frustrating at times, especially for beginners. Not only do you have to learn navigation, but also to be familiar with commands and their calling syntax*, and to understand concepts about the shell-specific scripting language (for example, Bash*). Decent typing skills aren't explicitly required but do make using the command line simpler. However, accurate spelling is essential, as are capitalization and file paths, so pay attention to the finer points. Given its complexities, it is comforting to know there are many modern tools and plugins to make using CLI more user-friendly.

I want to introduce you to two such tools that have helped me immensely with CLI interaction. Once you get the hang of it, Autojump* will be your best friend! It will be one of the first things you install when setting up a new system. Used in conjunction with `ag`*, a recursive filesystem search tool, you’ll be zooming around in no time. I'll cover basic use, but the man pages and documentation can introduce you to even more functionality. In this guide, I'll concentrate on installation and set-up.

Let's say you wanted to modify a file in the 'Pop' directory found at `/usr/share/gnome-shell/theme/Pop`. Normally you'd have to type `cd /usr/share/gnome-shell/theme/Pop` to find your file. If you've got Autojump installed, and have navigated to that directory at least once before, it is a matter of typing `j Pop`, and you'll be taken directly to it. How practical is that! Hang on, how did you know your file was even in the `/Pop` directory? Did you sift through the system using `cd` everywhere? Did you look through all of `/usr` and then all of `/share`, and who knows where else, poking around? If so, a CLI search tool is quite handy. There's a few with slightly different functionality, but you can't beat `ag`! Known as "The Silver Searcher", `ag` searches the directory you're currently in recursively, meaning... it's fast and comprehensive! To learn more, visit its homepage* or website*, but right now, we're going to install it and demonstrate some basics.

[bold]Installing Autojump and `ag`- The Silver Searcher;[/bold]

Unlike other distributions, the System76 developers opted to include both of these wonderful tools in Pop's software repository, so installing them is as simple as typing `sudo apt install autojump silversearcher-ag`. Now, `ag` is ready to use, but Autojump needs a couple more steps. If you installed via `apt` the Autojump site says;

>All Debian-derived distros require manual activation for policy reasons, please see `/usr/share/doc/autojump/README.Debian`.

Navigating to and opening* `README.Debian` instructs:

```
To use autojump, you need to configure your shell to source /usr/share/autojump/autojump.sh on startup.

If you use Bash, add the following line to your ~/.bashrc ...:
. /usr/share/autojump/autojump.sh
```
There are some clever ways of combining commands and options to add the line where it needs to go, but being a beginner guide, I want to show you an essential skill - editing a file with an editor. The default editor that comes packaged in Pop is "Nano"*. Use the command `cd` to return to your `/home` folder - where `.bashrc` lives. In Terminal type `nano ~/.bashrc` and you should now see a window like the one below.

_Picture of Nano with the appended line and comment_.

Now add the required information in a suitable place. Be careful not to disturb any existing code blocks. Sometimes placement defines the order in which code executes, so if the word "append" is used, make sure to add it to the end of the file. In this instance, placement is less critical. It's also good practice to document any changes you make to configuration files with comments. In shell script, comments are any lines that start with a `#` (see the picture above). To save changes in Nano, press the control key plus the "s" key - (ctrl + s). To exit Nano press ctrl + x. Finally, close Terminal and start again for `~/.bashrc` to regenerate with the added information. You're almost ready to use Autojump, but remember I said that you had to have visited the directory at least once before Autojump will take you there? It's my custom to start "preloading" the directories I often use by merely navigating to them.

_Below is one example of how to get Autojump ready to work for you_

To test just type `j doc`, `j dow`, `j mu` or `j pic`. Autojump should be taking you to these folders. If not, you've done something incorrectly and should review the process. If you've been successful, you can use the `ls`, or "list" command to see the contents of the current directory. To see hidden files, use `ls -a`, or use `ls -l` for extra information. To see more information on any command's use and options, use `man` (manual or docs). For example: `man ls`, `man cd`, `man ag` or `man autojump`. For a more comprehensive tutorial on Autjump usage, see this* article.

Searching with `ag` can be simple or as complex as you like. The most fundamental use is: Navigate to the directory you need to be in and run `ag -i <search term>`. The `-i` option forces `ag` to ignore case. See the picture below for a bit more detail.

All that's left to do is join the dots. You can use `ag` to find the directories you want quickly, and once navigated to, Autojump can get you there promptly in the future. Simply copy/paste from `ag` output and use it with `cd`. Alternatively, you can use Autojump to quickly get to any directory where you want to search for a file, or whatever term you think will give you the result you want. Autojump and The Silver Searcher are excellent examples of how modern CLI tools can complement each other and increase your productivity.

For a more comprehensive tutorial on `ag` usage, see this* article.
For a more comprehensive tutorial on Autjump usage, see this* article.



https://github.com/wting/autojump
https://en.wikipedia.org/wiki/Syntax
https://en.wikipedia.org/wiki/Bash_shell
https://github.com/ggreer/the_silver_searcher
https://geoff.greer.fm/ag/
https://en.wikipedia.org/wiki/GNU_nano
https://en.wikipedia.org/wiki/Cat_(Unix)
https://www.tecmint.com/autojump-a-quickest-way-to-navigate-linux-filesystem/
https://www.tecmint.com/the-silver-searcher-a-code-searching-tool-for-linux/
